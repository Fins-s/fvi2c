/*******************************************************************************
* @file    fvi2c_master.h
* @author  Fins
* @brief   I2c master core file
* @time    2020-10-22 20:23:33 Thursday
* @codeing UTF-8
* @license
*     Copyright 2020 Fins
*
*     Licensed under the Apache License, Version 2.0 (the "License");
*     you may not use this file except in compliance with the License.
*     You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*     Unless required by applicable law or agreed to in writing, software
*     distributed under the License is distributed on an "AS IS" BASIS,
*     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*     See the License for the specific language governing permissions and
*     limitations under the License.
* @verbatim
================================================================================
                        ##### xxxxxxxxxxxxxxxxxxxxxxx #####
================================================================================
* @endverbatim
* @attention
*******************************************************************************/



/** Prevent recursive inclusion */
#ifndef __FVI2C_MASTER_H
#define __FVI2C_MASTER_H

/** C++ compatible */
#ifdef __cplusplus
 extern "C" {
#endif /** __cplusplus */

/* Includes start *************************************************************/
/*     Lib files ****************************** */

/*     User files ***************************** */
#include "../common/fvi2c_def.h"
#include "../common/fvi2c_config.h"
#include "../common/fvi2c_debug.h"

/* Includes end ***************************************************************/

/* Exported define start ******************************************************/

#define FVI2C_NEW_MSG(mName, mAddr, Flags, mLen, mBuffer)                      \
    fvi2c_msg_s mName = {.addr = (mAddr), .flags = (mFlags),                   \
    .len = (mLen), .msg = (mBuffer)}

#define FVI2C_NEW_CONF(cName, hPer, hPos, cPer, cPos)                          \
    fvi2c_conf_s cName = {.holdPerTime = (hPer), .holdPosTime = (hPos),        \
    .chgPerTime = (cPer), .chgPosTime = (cPos)}

#define FVI2C_NEW_IF(iName, getPinF, setPinF, unitD)                           \
    fvi2c_lLIf_s iName = {.GetPin = (getPinF), .SetPin = (setPinF),            \
    .UnitDelay = (unitD)}

/* Exported define end ********************************************************/

/* Exported typedef start *****************************************************/





/* Exported typedef end *******************************************************/

/* Exported variable start ****************************************************/





/* Exported variable end ******************************************************/

/* Exported function start ****************************************************/

extern fvi2c_ret_e Fvi2cConfig(fvi2c_s* vi2cP,const fvi2c_conf_s* conf);
extern fvi2c_ret_e Fvi2cAttachIf(fvi2c_s* vi2cP,const fvi2c_lLIf_s* lLIf);
extern fvi2c_ret_e Fvi2cMasTransfer(fvi2c_s* vi2cP,
    fuint32 num,
    fvi2c_msg_s msgs[]);

/* Exported function end ******************************************************/

/** C++ compatible */
#ifdef __cplusplus
}
#endif /** __cplusplus */

#endif /** __FVI2C_MASTER_H */
////////////////////////////////- END OF FILE  -////////////////////////////////
