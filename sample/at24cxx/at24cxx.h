/*******************************************************************************
* @file    at24cxx.h
* @author  Fins
* @brief   
* @time    2020-11-05 21:52:09 Thursday
* @codeing UTF-8
* @license
*     Copyright 2020 Fins
*
*     Licensed under the Apache License, Version 2.0 (the "License");
*     you may not use this file except in compliance with the License.
*     You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*     Unless required by applicable law or agreed to in writing, software
*     distributed under the License is distributed on an "AS IS" BASIS,
*     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*     See the License for the specific language governing permissions and
*     limitations under the License.
* @verbatim
================================================================================
                        ##### xxxxxxxxxxxxxxxxxxxxxxx #####
================================================================================
* @endverbatim
* @attention
*******************************************************************************/



/** Prevent recursive inclusion */
#ifndef __AT24CXX_H
#define __AT24CXX_H

/** C++ compatible */
#ifdef __cplusplus
 extern "C" {
#endif /** __cplusplus */

/* Includes start *************************************************************/
/*     Lib files ****************************** */

/*     User files ***************************** */
#include "stm32h7xx_hal.h"
#include "../../fvi2c.h"

/* Includes end ***************************************************************/

/* Exported define start ******************************************************/

/*************************************************
 * @brief  I2C config
 ************************************************/
#define SCL_PIN GPIO_PIN_5
#define SCL_GPIO_Port GPIOA
#define SDA_PIN GPIO_PIN_7
#define SDA_GPIO_Port GPIOA

/*************************************************
 * @brief AT24CXX config
 ************************************************/
#define AT24CXX_ADDR          0xA0
#define AT24CXX_I2C_MOD       (FVI2C_8B_ADDR | FVI2C_OLADDR | FVI2C_READ_ACK | \
                               FVI2C_GERR_NACK)
#define AT24CXX_I2C_R_CONF(msgLen,msgBuff) {     \
    .addr = AT24CXX_ADDR,                        \
    .flags = (FVI2C_READ | AT24CXX_I2C_MOD),     \
    .len = (msgLen),                             \
    .msg = (msgBuff),                            \
}
#define AT24CXX_I2C_W_CONF(msgLen,msgBuff) {     \
    .addr = AT24CXX_ADDR,                        \
    .flags = (FVI2C_WRITE | AT24CXX_I2C_MOD),    \
    .len = (msgLen),                             \
    .msg = (msgBuff),                            \
}     

#define AT24CXX_T_BUFF_SIZE     64

/* Exported define end ********************************************************/

/* Exported typedef start *****************************************************/





/* Exported typedef end *******************************************************/

/* Exported variable start ****************************************************/





/* Exported variable end ******************************************************/

/* Exported function start ****************************************************/

extern fuint8 At24cxxInit(void);
extern fvi2c_ret_e At24cxxMulRead(fuint16 sAddr, fuint16 num, fuint8* buff);
extern fvi2c_ret_e At24cxxMulWrite(fuint16 sAddr, fuint16 num, const fuint8* buff);


/* Exported function end ******************************************************/

/** C++ compatible */
#ifdef __cplusplus
}
#endif /** __cplusplus */

#endif /** __AT24CXX_H */
////////////////////////////////- END OF FILE  -////////////////////////////////
