/*******************************************************************************
* @file    at24cxx.c
* @author  Fins
* @brief   
* @time    2020-11-05 21:52:35 Thursday
* @codeing UTF-8
* @license
*     Copyright 2020 Fins
*
*     Licensed under the Apache License, Version 2.0 (the "License");
*     you may not use this file except in compliance with the License.
*     You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*     Unless required by applicable law or agreed to in writing, software
*     distributed under the License is distributed on an "AS IS" BASIS,
*     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*     See the License for the specific language governing permissions and
*     limitations under the License.
* @verbatim
================================================================================
                        ##### xxxxxxxxxxxxxxxxxxxxxxx #####
================================================================================
* @endverbatim
* @attention
*******************************************************************************/



/* Includes start *************************************************************/
/*     Lib files ****************************** */

/*     User files ***************************** */
#include "at24cxx.h"

/* Includes end ***************************************************************/

/* Define start ***************************************************************/



/* Define end *****************************************************************/

/* Typedef start **************************************************************/



/* Typedef end ****************************************************************/

/* Global variable start ******************************************************/

/*************************************************
 * @brief Vi2c low leval interface
 ************************************************/
extern fvi2c_pSta_e Vi2cGetPin(fvi2c_pin_e pin);
extern void Vi2cSetPin(fvi2c_pin_e pin, fvi2c_pSta_e status);
extern void Vi2cUnitDelay(void);

fvi2c_s vi2c;                 /**< I2c instance */

/* Global Variable end ********************************************************/

/* Static function declaration start ******************************************/



/* Static function declaration end ********************************************/

/* function start *************************************************************/

/**
  * @brief      At24cxx initialize
  * @param[in]  None
  * @retval     None
  */
fuint8 At24cxxInit(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOA_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOA, SCL_PIN|SDA_PIN, GPIO_PIN_SET);

    /*Configure GPIO pins : PA5 PA7 */
    GPIO_InitStruct.Pin = SCL_PIN|SDA_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(SCL_GPIO_Port, &GPIO_InitStruct);

    FVI2C_NEW_IF(vi2cInterface, Vi2cGetPin, Vi2cSetPin, Vi2cUnitDelay);
    FVI2C_NEW_CONF(vi2cConf, 1, 1, 1, 1);
    Fvi2cAttachIf(&vi2c, &vi2cInterface);
    Fvi2cConfig(&vi2c, &vi2cConf);

    return 0;
}

/**
  * @brief      At24cxx multiple read
  * @param[in]  sAddr: start address
  * @param[in]  num: read number
  * @param[out] buff: data buffer
  * @retval     Refer 'fvi2c_ret_e'
  */
fvi2c_ret_e At24cxxMulRead(fuint16 sAddr, fuint16 num, fuint8* buff)
{
    FVI2C_ASSERT(buff != F_NULL);

    fuint8 msg1Data[2] = {sAddr >> 8, sAddr};
    fvi2c_msg_s msgs[2] = 
    {
        AT24CXX_I2C_W_CONF(sizeof(msg1Data)/sizeof(msg1Data[0]),msg1Data),
        AT24CXX_I2C_R_CONF(num,buff),
    };
    return Fvi2cMasTransfer(&vi2c,2,msgs);
}



/**
  * @brief      At24cxx multiple write
  * @param[in]  sAddr: Var brief
  * @param[in]  num: write number
  * @param[in]  buff: data buffer
  * @retval     None
  * @retval     Refer 'fvi2c_ret_e'
  */
fuint8 writeBuff[AT24CXX_T_BUFF_SIZE];
fvi2c_ret_e At24cxxMulWrite(fuint16 sAddr, fuint16 num, const fuint8* buff)
{
    FVI2C_ASSERT(buff != F_NULL);

    fuint8 buffIndex = 0;
    writeBuff[buffIndex++] = sAddr >> 8;
    writeBuff[buffIndex++] = sAddr;

    for(fuint8 i = 0; i < num; i++)
    {
        writeBuff[buffIndex++] = buff[i];
    }
    fvi2c_msg_s msgs = AT24CXX_I2C_W_CONF(buffIndex,writeBuff);
    return Fvi2cMasTransfer(&vi2c,1,&msgs);
}


/*************************************************
 * @brief Low leval interface function
 ************************************************/

/**
  * @brief      Get pin status, vi2c low leval interface
  * @param[in]  pin: 'FVI2C_SCL / FVI2C_SDA'
  * @retval     'FVI2C_PIN_LOW / FVI2C_PIN_HIGH'
  */
fvi2c_pSta_e Vi2cGetPin(fvi2c_pin_e pin)
{
    if (pin == FVI2C_SCL) 
        return (fvi2c_pSta_e)HAL_GPIO_ReadPin(SCL_GPIO_Port, SCL_PIN);
    else
        return (fvi2c_pSta_e)HAL_GPIO_ReadPin(SDA_GPIO_Port, SDA_PIN);
}

/**
  * @brief      Set pin status, vi2c low leval interface
  * @param[in]  pin: 'FVI2C_SCL / FVI2C_SDA'
  * @param[in]  status: 'FVI2C_PIN_LOW / FVI2C_PIN_HIGH'
  * @retval     None
  */
void Vi2cSetPin(fvi2c_pin_e pin, fvi2c_pSta_e status)
{
    if (pin == FVI2C_SCL)
        HAL_GPIO_WritePin(SCL_GPIO_Port, SCL_PIN, status);
    if (pin == FVI2C_SDA)
        HAL_GPIO_WritePin(SDA_GPIO_Port, SDA_PIN, status);
}

/**
  * @brief      Delay function, delay unit time, vi2c low leval interface, vi2c
  *             cycle = 2 * (unit time * sdaPerTime) + (unit time * sdaPosTime)
  * @param[in]  None
  * @retval     None
  */
void Vi2cUnitDelay(void)
{
    for(int i = 0; i < 480 ; i++);
}

/* function end ***************************************************************/

////////////////////////////////- END OF FILE  -////////////////////////////////
