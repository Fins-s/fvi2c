/*******************************************************************************
* @file    fvi2c_def.h
* @author  Fins
* @brief   Type define file
* @time    2020-10-11 13:33:42 Sunday
* @codeing UTF-8
* @license
*     Copyright 2020 Fins
*
*     Licensed under the Apache License, Version 2.0 (the "License");
*     you may not use this file except in compliance with the License.
*     You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*     Unless required by applicable law or agreed to in writing, software
*     distributed under the License is distributed on an "AS IS" BASIS,
*     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*     See the License for the specific language governing permissions and
*     limitations under the License.
* @verbatim
================================================================================
                        ##### xxxxxxxxxxxxxxxxxxxxxxx #####
================================================================================
* @endverbatim
* @attention
*******************************************************************************/



/** Prevent recursive inclusion */
#ifndef __FVI2C_DEF_H
#define __FVI2C_DEF_H

/** C++ compatible */
#ifdef __cplusplus
 extern "C" {
#endif /** __cplusplus */

/* Includes start *************************************************************/
/*     Lib files ****************************** */

/*     User files ***************************** */


/* Includes end ***************************************************************/

/* Exported define start ******************************************************/

#ifndef USE_F_COMMON_DEFINE
/*************************************************
 * @brief Rename basic type
 ************************************************/
typedef unsigned long long     fuint64;           /**< unsigned int 64 */
typedef unsigned int           fuint32;           /**< unsigned int 32 */
typedef unsigned short         fuint16;           /**< unsigned int 16 */
typedef unsigned char          fuint8;            /**< unsigned int 08 */

typedef signed long long       fsint64;           /**< signed int 64   */
typedef signed int             fsint32;           /**< signed int 32   */
typedef signed short           fsint16;           /**< signed int 16   */
typedef signed char            fsint8;            /**< signed int 08   */

typedef float                  ffloat32;          /**< signed float 32 */
typedef double                 ffloat64;          /**< signed float 64 */

typedef fuint32                fbool;             /**< boolean         */

typedef fuint32                fstate;            /**< state           */

typedef fuint64                faddr;             /**< addr            */

/*************************************************
 * @brief Minium value of base type
 ************************************************/
#define FUINT8_MIN      0                         /**< Minium value of UINT8  */
#define FUINT16_MIN     0                         /**< Minium value of UINT16 */
#define FUINT32_MIN     0                         /**< Minium value of UINT32 */
#define FUINT64_MIN     0                         /**< Minium value of UINT64 */

#define FSINT8_MIN      0x80                      /**< Minium value of SINT8  */
#define FSINT16_MIN     0x8000                    /**< Minium value of SINT16 */
#define FSINT32_MIN     0x80000000                /**< Minium value of SINT32 */
#define FSINT64_MIN     0x8000000000000000        /**< Minium value of SINT64 */

/*************************************************
 * @brief Machine word size
 ************************************************/
#define F_MACHINE_WORDSIZE             (sizeof(int*))

/*************************************************
 * @brief Define NULL
 ************************************************/
#ifndef NULL
#define NULL    ((void*)0)                        /**< Empty pointer */
#endif

#ifndef F_NULL
#define F_NULL  ((void*)0)                        /**< Empty pointer */
#endif

/*************************************************
 * @brief Macros depending on compiler
 ************************************************/
#if defined ( __CC_ARM   )                        /**< ARM Compiler  */
  #define F_INLINE         __inline

#elif defined ( __ICCARM__ )                      /**< IAR Compiler  */
  #define F_INLINE         inline    
  #define F_STATIC_INLINE  static inline   
  
#elif defined   (  __GNUC__  )                    /**< GNU Compiler  */
  #define F_INLINE         inline     
  #define F_STATIC_INLINE  static inline    

#elif defined   (  __TASKING__  )                 /**< TASKING Compiler  */
  #define F_INLINE         inline          

#endif

#endif /* USE_F_COMMON_DEFINE */





/* Exported define end ********************************************************/

/* Exported typedef start *****************************************************/

/*************************************************
 * @brief Virtual I2C return value
 ************************************************/
typedef enum fvi2c_ret
{
    FVI2C_ROK = 0,       /**< Ok                */
    FVI2C_RERR,          /**< Error             */
    FVI2C_RNACK,         /**< Device NACK       */
    FVI2C_RBUS_BUSY,     /**< I2C bus busy      */
    FVI2C_RARB_FAIL,     /**< Arbitrate fail    */
    FVI2C_RTIMEOUT,      /**< time out          */
}fvi2c_ret_e;

/*************************************************
 * @brief Virtual I2C acknowladge status
 ************************************************/
typedef enum fvi2c_ack
{
    FVI2C_ACK = 0,     /**< ACK */
    FVI2C_NACK,        /**< NACK */
}fvi2c_ack_e;

/*************************************************
 * @brief Virtual I2C pin
 ************************************************/
typedef enum fvi2c_pin
{
    FVI2C_SCL = 0,     /**< SCL pin */
    FVI2C_SDA,         /**< SDA pin */
}fvi2c_pin_e;

/*************************************************
 * @brief Virtual I2C pin status
 ************************************************/
typedef enum fvi2c_pSta
{
    FVI2C_PIN_LOW = 0,     /**< Pin low  */
    FVI2C_PIN_HIGH,        /**< Pin high */
}fvi2c_pSta_e;

/*************************************************
 * @brief Virtual I2C flags
 ************************************************/
typedef enum fvi2c_flag
{
    FVI2C_8B_ADDR     = 0U,        /**< 8bit address (default)                */
    FVI2C_10B_ADDR    = 1U,        /**< 10bit address                         */
    FVI2C_RWADDR      = 0U,        /**< ADDR with RW bit, RW flag no effect(d)*/
    FVI2C_OLADDR      = (1U << 1), /**< ADDR without RW bit, RW flag valid    */
    FVI2C_WRITE       = 0U,        /**< Write flag (default)                  */
    FVI2C_READ        = (1U << 2), /**< Read flag                             */
    FVI2C_READ_ACK    = 0U,        /**< ACK when read (default)               */
    FVI2C_READ_NACK   = (1U << 3), /**< NACK when read                        */
    FVI2C_GERR_NACK   = 0U,        /**< Generate error when get NACK (default)*/
    FVI2C_IGNORE_NACK = (1U << 4), /**< ignore when get NACK                  */
}fvi2c_flag_e;

/*************************************************
 * @brief Virtual I2C low level interface (user 
 *        achieve)
 ************************************************/
typedef struct fvi2c_lLInt
{
    fvi2c_pSta_e (*GetPin)(fvi2c_pin_e pin);              /**< Get pin status */
    void (*SetPin)(fvi2c_pin_e pin, fvi2c_pSta_e status); /**< Set pin status */
    void (*UnitDelay)(void);                              /**< Unit time delay*/
}fvi2c_lLIf_s;

/*************************************************
 * @brief Virtual I2C instance 
 ************************************************/
typedef struct fvi2c_conf
{
    fuint32 holdPerTime;  /**< Hold stage preposition time    */
    fuint32 holdPosTime;  /**< Hold stage postposition time   */
    fuint32 chgPerTime;   /**< Change stage preposition time  */
    fuint32 chgPosTime;   /**< Change stage postposition time */
}fvi2c_conf_s;

/*************************************************
 * @brief Virtual I2C instance 
 ************************************************/
typedef struct fvi2c
{
    fvi2c_lLIf_s interface; /**< Low level interface */
    fvi2c_conf_s conf;      /**< Config */
}fvi2c_s;

/*************************************************
 * @brief Virtual I2C massage
 ************************************************/
typedef struct fvi2c_msg
{
    fuint16 addr;          /**< I2C device address 8 bit                      */
    fvi2c_flag_e flags;    /**< Flags refer 'fvi2c_flag_e' use '|' enable 
                                multiple flags                                */
    fuint32 len;           /**< Massage length when transmit, read number when 
                                recieve, unit: byte                           */
    fuint8* msg;           /**< Transmit datas or recieve buffer              */
}fvi2c_msg_s;

/* Exported typedef end *******************************************************/

/* Exported variable start ****************************************************/





/* Exported variable end ******************************************************/

/* Exported function start ****************************************************/





/* Exported function end ******************************************************/

/** C++ compatible */
#ifdef __cplusplus
}
#endif /** __cplusplus */

#endif /** __FVI2C_DEF_H */
////////////////////////////////- END OF FILE  -////////////////////////////////
