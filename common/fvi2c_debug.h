/*******************************************************************************
* @file    fvi2c_debug.h
* @author  Fins
* @brief   Debug define file
* @time    2020-10-30 20:48:56 Friday
* @codeing UTF-8
* @license
*     Copyright 2020 Fins
*
*     Licensed under the Apache License, Version 2.0 (the "License");
*     you may not use this file except in compliance with the License.
*     You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*     Unless required by applicable law or agreed to in writing, software
*     distributed under the License is distributed on an "AS IS" BASIS,
*     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*     See the License for the specific language governing permissions and
*     limitations under the License.
* @verbatim
================================================================================
                        ##### xxxxxxxxxxxxxxxxxxxxxxx #####
================================================================================
* @endverbatim
* @attention
*******************************************************************************/



/** Prevent recursive inclusion */
#ifndef __HFILE_H
#define __HFILE_H

/** C++ compatible */
#ifdef __cplusplus
 extern "C" {
#endif /** __cplusplus */

/* Includes start *************************************************************/
/*     Lib files ****************************** */

/*     Lib files ****************************** */
#include "fvi2c_config.h"

/* Includes end ***************************************************************/

/* Exported define start ******************************************************/


#if FVI2C_USE_FULL_ASSERT
/*************************************************
 * @brief Debug macro
 *     Sample:
 *         #define FVI2C_ASSERT(expr)            \
 *             ((exper) ? (void)0U : printf(     \
 *             "ASSERT FAILURE(%s): %s,%d\r\n",  \
 *             #exper,__FUNC__,__LINE__))
 ************************************************/

#ifndef FVI2C_ASSERT
#error Assert macro undefine
#endif /* FCI2C_ASSERT(expr) */

#else  /* FVI2C_USE_FULL_ASSERT */
#define FVI2C_ASSERT(expr)              ((void)0U)
#endif /* FVI2C_USE_FULL_ASSERT */

/* Exported define end ********************************************************/

/* Exported typedef start *****************************************************/





/* Exported typedef end *******************************************************/

/* Exported variable start ****************************************************/





/* Exported variable end ******************************************************/

/* Exported function start ****************************************************/





/* Exported function end ******************************************************/

/** C++ compatible */
#ifdef __cplusplus
}
#endif /** __cplusplus */

#endif /** __HFILE_H */
////////////////////////////////- END OF FILE  -////////////////////////////////
