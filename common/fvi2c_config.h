/*******************************************************************************
* @file    fvi2c_config.h
* @author  Fins
* @brief   Global config file
* @time    2020-10-11 14:32:33 Sunday
* @codeing UTF-8
* @license
*     Copyright 2020 Fins
*
*     Licensed under the Apache License, Version 2.0 (the "License");
*     you may not use this file except in compliance with the License.
*     You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*     Unless required by applicable law or agreed to in writing, software
*     distributed under the License is distributed on an "AS IS" BASIS,
*     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*     See the License for the specific language governing permissions and
*     limitations under the License.
* @verbatim
================================================================================
                        ##### xxxxxxxxxxxxxxxxxxxxxxx #####
================================================================================
* @endverbatim
* @attention
*******************************************************************************/



/** Prevent recursive inclusion */
#ifndef __FVI2C_CONFIG_H
#define __FVI2C_CONFIG_H

/** C++ compatible */
#ifdef __cplusplus
 extern "C" {
#endif /** __cplusplus */

/* Includes start *************************************************************/
/*     Lib files ****************************** */

/*     User files ***************************** */


/* Includes end ***************************************************************/

/* Exported define start ******************************************************/

#define FVI2C_WPIN_TIMEOUT      500U /**< Wait scl high read pin status times */

#define FVI2C_USE_FULL_ASSERT   0U   /**< 1: Use Assert 0: Unuse              */   

/* Exported define end ********************************************************/

/* Exported typedef start *****************************************************/





/* Exported typedef end *******************************************************/

/* Exported variable start ****************************************************/





/* Exported variable end ******************************************************/

/* Exported function start ****************************************************/





/* Exported function end ******************************************************/

/** C++ compatible */
#ifdef __cplusplus
}
#endif /** __cplusplus */

#endif /** __FVI2C_CONFIG_H */
////////////////////////////////- END OF FILE  -////////////////////////////////